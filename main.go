package main

import (
	"bufio"
	"container/heap"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

func main() {
	fileName := flag.String("file", "", "Path to a file with integer numbers on each line.")
	N := flag.Int("n", 5, "Number of ")
	bufferSize := flag.Int("buffer", 0, "Size of the buffer to use when reading the file. Defaults to Golang's automatic buffering.")
	flag.Parse()
	if *fileName == "" {
		log.Fatal(("Please provide a file path with the -file option"))
	}
	fmt.Printf("Reading from %s...\n", *fileName)

	maxNumbers := make(priorityQueue, *N)
	heap.Init(&maxNumbers)

	file, err := os.Open(*fileName)
	if err != nil {
		log.Fatalf("Failed to open file: %v", err)
	}

	// io.Reader automatically buffers as it reads from the file, so it
	// will not exceed the memory limit of the machine/process. I am allowing
	// it to automatically determine the buffer size, or it can be overriden
	// with the -buffer option
	var bufferedReader io.Reader
	if *bufferSize > 0 {
		fmt.Printf("Setting buffer size to %d bytes\n", *bufferSize)
		bufferedReader = bufio.NewReaderSize(file, *bufferSize)
	} else {
		bufferedReader = file
	}

	// I'm utilizing Golang's bufio.Scanner object which will
	// automatically read the file, and allow me to iterate over each line.
	scanner := bufio.NewScanner(bufferedReader)
	for scanner.Scan() {
		line := scanner.Text()
		number, err := strconv.Atoi(line)
		if err != nil {
			log.Printf("Failed to read '%s' as an integer. Skipping...\n", line)
			continue
		}

		heap.Push(&maxNumbers, number)
		// We don't want the heap to actually grow,
		// just stay at size N. So we Pop()
		heap.Pop(&maxNumbers)
	}

	var displayNumbers = make([]int, *N)
	for maxNumbers.Len() > 0 {
		val := heap.Pop(&maxNumbers).(int)
		displayNumbers[maxNumbers.Len()] = val
	}
	fmt.Printf("Largest %d numbers in file:\n", *N)
	for _, num := range displayNumbers {
		fmt.Println(num)
	}
}

// priorityQueue is a quick implementation of a
// Priority Queue using golang's container/heap
type priorityQueue []int

// Len returns the length of the queue
// Provided to satisfy container/heap interface
func (queue priorityQueue) Len() int {
	return len(queue)
}

// Less determines comparison of objects in the queue
// Provided to satisfy container/heap interface
func (queue priorityQueue) Less(i, j int) bool {
	return queue[i] < queue[j]
}

// Swap swaps two elements in the queue
// Provided to satisfy container/heap interface
func (queue priorityQueue) Swap(i, j int) {
	queue[i], queue[j] = queue[j], queue[i]
}

// Push appends an object to the queue
// Provided to satisfy container/heap interface
func (queue *priorityQueue) Push(x interface{}) {
	*queue = append(*queue, x.(int))
}

//Pop removes the smallest item from the queue
// and returns it.
// Provided to satisfy container/heap interface
func (queue *priorityQueue) Pop() interface{} {
	n := len(*queue)
	item := (*queue)[n-1]
	*queue = (*queue)[0 : n-1]
	return item
}
