# GitLab Code

This is just a quick little repository to answer question #5 for the GitLab interview process.

## Overview of code

The code begins by streaming in the input file using a buffered reader. As it does so, it reads each line of the file and adds the corresponding integer to a Priority Queue. This queue's size is intentionally limited to N.

Once the entire file has been read and fed to the queue, only the N largest numbers will remain in the queue. Golang's `container/heap` implementation is used to optimize the priority queue's performance.

Then, the remaining N elements from the queue can be printed to the screen.

## Runtime analysis and time/space complexity

In order to determine the algorithmic complexity of the program, I'm going to define a couple of variables.

- `n`: the size of the input file in bytes
  - technically, we could measure this in lines. But bytes will be a more accurate metric since the vast majority of the time is spent in IO operations.
- `N`: the number of largest integers to find in the file.

#### Time Complexity

The overall Big O time complexity of the program is `O(n * log N)`. The program must read all bytes in the input file, and for each (roughly constant) number of bytes, it must call `Push()` and `Pop()` on the priority queue. Both `Push()` and `Pop()` are `O(log N)` operations. There is also a `O(N)` component that is required to print the `N` largest numbers. However, this will not affect the overall Big O complexity since we can assume that `N` is always smaller than `n`

- **Note**: Since the time spent in IO operations vastly outweighs the time required to handle the Priority Queue, the `log N` component of the time complexity becomes largely negligible. Unfortunately, this is a limitation of Big O notation: it considers all operations to be equal.

#### Space Complexity

The Big O space complexity of the program is a simple `O(N)`. Since we are using a buffered reader to read from the input file, we do not need to load the entire file into memory. Rather, a constant amount of memory is used to load arbitrarily large files. I've made the buffer size configurable in the script, but it is **entirely independent** of the size of `n`. We do however, have to allocate a Priority Queue with enough space to store the `N` largest numbers. Therefore, the space complexity **is** linearly dependent on `N`.

#### Optimality

I believe the Big O **space** complexity of the algorithm is optimal. There is no way to avoid storing the largest numbers that are desired for output. Thus there will always be a `O(N)` space complexity at minimum. However, I believe that the **time** complexity could be reduced. Using a heavily-modified version of the radix sorting algorithm might be able to produce a `O(n + N)` solution, or close to it. However, this type of implementation might increase the space complexity. I will investigate this approach in the [`radix` branch](https://gitlab.com/steven.thomas/interview-questions/tree/radix).

There are also plenty of other ways to improve the script. Naturally, the code itself can be iterated upon. Even if the same general approach is used, I still think there could be a number of performance improvements made. I think the memory footprint of the program could definitely be reduced. And there are certainly optimizations that could be used to immediately ignore certain values if they are too small (rather than relying on the slower Priority Queue).


## Running the code

In order to run the `main.go` program, you'll need a file with integer numbers on each line. You can generate a file in the correct format using the `testing/generatefile.go` script. More instructions in its [readme](./testing/README.md)

Then, you can just call the `main.go` script:

```
$ go run main.go -file test.txt -n 5
> Reading from test.txt...
> Largest 5 numbers in file:
> 9223371911506448592
> 9223369864194662078
> 9223368018057582213
> 9223367628014278483
> 9223366829941107345
```

- the `-file` option specifies the path to the file you are searching through.
- the `-n` option specifies how many largest numbers you would like to find.