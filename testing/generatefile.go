package main

import (
	"flag"
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	fileName := flag.String("file", "", "Path of the file you would like generated. This script will simply append to this file.")
	numMBytes := flag.Int("mbytes", 100, "Number of MB you would like added to the file. Defaults to 100MB")
	flag.Parse()
	if *fileName == "" {
		log.Fatal(("Please provide a file path with the -file option"))
	}

	file, err := os.OpenFile(*fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("Failed to open file: %v", err)
	}

	remainingBytes := *numMBytes * 1024 * 1024
	for remainingBytes > 0 {
		str := strconv.Itoa(rand.Int()) + "\n"
		file.WriteString(str)

		remainingBytes -= len(str)
	}
}
