## Generating a file for testing

To test this program, you'll need to generate a file with a single integer number on each line. This file can have an arbitrary number of lines, and can be arbitrarily big (well, assuming you can store it).

The `generatefile.go` script is simply a helper script to generate files in the correct format. It can be invoked as follows:

```
go run testing/generatefile.go -file ./numbers.txt -mbytes 100
```

- the `-file` option specifies what location to generate the file in.
- the `-mbytes` option specifies how many MBs of data to generate. Defaults to 100MB.